import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class ReadingFromFile {

    public static void main(String[] args) throws IOException {
        
        String fileName = "C:\\Program Files\\Java2\\test.txt";

        Reader fr = new FileReader(fileName);

        BufferedReader br = new BufferedReader(fr);

        try {
            String line;
            while ((line = br.readLine())!=null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            System.out.println("Error");
        } finally {
            br.close();
        }
    }
}

